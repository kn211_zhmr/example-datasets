Variable name; Variable value
Object ID (tensile test specimen); L4_500_10-1
Value of diameter (tensile test specimen); 3.13
Unit symbol of diameter (tensile test specimen); mm
Material name; CuNi14Al1Ge1.2
Comment (process evaluation of specimen preparation); OK
IWM short name (operator tensile test); ott
Date time end of process (yyyy-mm-dd hh:mm:ss); 2018-12-14
Date time start of process (yyyy-mm-dd hh:mm:ss); 2018-12-14
Name tensile testing machine; Z100HT
Name load cell;
Name optical strain measurement system; ZwickRoell
Value strain rate in loading direction (process parameter); 0.0067
Unit symbol strain rate in loading direction (process parameter); 1/s
Value (crosshead separation rate, process parameter); 0
Unit symbol (crosshead separation rate, process parameter);unit
Value original gauge length (process parameter); 17.682
Unit symbol original gauge length (process parameter); mm
Comment (after tensile test by operator); OK
File name xls file (raw data); ZV_500_10_xte051_0738_rawdata.txt
URL xls file (raw data); file://cudigit-server.iwm.fraunhofer.de/
Excel sheet name (Messdaten); ZV_500_10_xte051_0738_rawdata.txt
Number of header lines in xls sheet (raw data); 14
ColumnNumber TestingTime (raw data); 1
#Column number (ForceInLoadingDirection, raw data); 100
#Column number (optical displacement as is, raw data); 101
Column number (eng. strain in loading direction, raw data); 2
Column number (eng. stress in loading direction, raw data); 3
UnitSymbol TestingTime (raw data); s
#Unit symbol (ForceInLoadingDirection, raw data); kN
#Unit symbol (optical displacement as is, raw data file); mm
Unit symbol (eng. strain in loading direction, raw data); mm/mm
Unit symbol (eng. stress in loading direction, raw data); MPa
Value (Rp02); 619.749
Value (maximum tensile strength Rm); 815.482
Value (Youngs Modulus E); 133588.0
Unit symbol (Rp02, Rm, E); MPa
Value (percentage elongation after fracture, A); 0.214865
Unit symbol (percentage elongation after fracture, A); mm/mm